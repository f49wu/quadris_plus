# Quadris_plus

A C++ video game with solid objected-oriented design and various software design patterns (such as observer pattern and factory pattern)

Build the project: use `src/Makefile` to build project

`AI-model`: allow users compete with an AI, add flag `-AI-model` to start the game with AI model

please read `quadris_plus.pdf` for all features and design of this project

uml diagram:

![ScreenShot](./uml.jpg)

UI: <img src="./UI.png" width="40%"> AI mode: <img src="./AI_mode.png" width="47%">

basic commands:

`left`: moves the current block one cell to the left.

`right`: same as above, but to the right.

`down`: as above, but one cell downward.

`clockwise`: rotates the block 90 degrees clockwise, as described earlier.

`counterclockwise`: as above, but counterclockwise.

`drop`: drops the current block.

`levelup`: Increases the di culty level of the game by one.

`leveldown`: Decreases the di culty level of the game by one.

`norandom file`: Relevant only during levels 3 and 4, this command makes these levels non- random, instead taking input from the sequence file, starting from the beginning.

`random`: Relevant only during levels 3 and 4, this command restores randomness in these levels.

`sequence -file`: Executes the sequence of commands found in file.

`I, J, L, etc.`: Useful during testing, these commands replace the current undropped block with the stated block. restart: Clears the board and starts a new game.

`hint`: Suggests a landing place for the current block.