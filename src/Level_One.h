
#ifndef Level_One_h
#define Level_One_h

#include "level.h"
#include <vector>

class Block;
class Level;

class Level_One: public Level{
    
    std::vector<cell_block_type> sequence;
    cell_block_type _NextBlock() override;
    
public:
    Level_One(int level);
    ~Level_One();
};

#endif
