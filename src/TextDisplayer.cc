#include "TextDisplayer.h"
#include <iomanip>

using namespace std;

std::ostream &operator<<(std::ostream &out, const TextDisplay &td){
    
    int row = 18;
    int col = 11;
    int next_row = 2;
    int next_col = 4;
    
    out<<"Level: "<<setw(7)<<td.quadris->getLevel()<<endl;
    out<<"Score: "<<setw(7)<<td.quadris->getScore()<<endl;
    out<<"Hi Score: "<<setw(4)<<td.quadris->getHiScore()<<endl;
    
    
    for(int i = 0; i < col; i++){
        out<<"-";
    }
    
    out<<endl;
    
    for(int i = 0; i < row; i++){
        for(int j = 0; j < col; j++){
            out<<td.theDisplay[i][j];
        }
        out<<" "<<i<<endl;
    }
    
    for(int i = 0; i < col; i++){
        out<<"-";
    }
    
    out<<endl;
    
    out<<"Next:"<<endl;
    
    for(int i = 0; i < next_row; i++){
        for(int j = 0; j < next_col; j++){ 
            out<<td.NextBoard[i][j];
        }
        out<<endl;
    }
    
    
    return out;
}


void TextDisplay::game_over(){
    theDisplay[4][5] = 'G';
    theDisplay[5][5] = 'A';
    theDisplay[6][5] = 'M';
    theDisplay[7][5] = 'E';
    theDisplay[9][5] = 'O';
    theDisplay[10][5] = 'V';
    theDisplay[11][5] = 'E';
    theDisplay[12][5] = 'R';
}


void TextDisplay::clear(){
    
    for(auto& row: theDisplay){
        for(auto& c: row){
            c = ' ';
        }
    }
    
    for(auto& row: NextBoard){
        for(auto& c: row){
            c = ' ';
        }
    }
    
}


void TextDisplay::ShowNextBlock(cell_block_type type){
    
    for(auto &row: NextBoard){
        for(auto& cell: row){
            cell = ' ';
        }
    }
    
    if(type == cell_block_type::I){
        NextBoard[0][0] = 'I';
        NextBoard[0][1] = 'I';
        NextBoard[0][2] = 'I';
        NextBoard[0][3] = 'I';
    }else if(type == cell_block_type:: J){
        NextBoard[0][0] ='J';
        NextBoard[1][0] ='J';
        NextBoard[1][1] ='J';
        NextBoard[1][2] ='J';
    }else if(type == cell_block_type:: L){
        NextBoard[1][0] ='L';
        NextBoard[1][1] ='L';
        NextBoard[1][2] ='L';
        NextBoard[0][2] ='L';
    }else if(type == cell_block_type:: O){
        NextBoard[0][0] ='O';
        NextBoard[0][1] ='O';
        NextBoard[1][0] ='O';
        NextBoard[1][1] ='O';
    }else if(type == cell_block_type:: S){
        NextBoard[1][0] ='S';
        NextBoard[1][1] ='S';
        NextBoard[0][1] ='S';
        NextBoard[0][2] ='S';
    }else if(type == cell_block_type:: Z){
        NextBoard[0][0] ='Z';
        NextBoard[0][1] ='Z';
        NextBoard[1][1] ='Z';
        NextBoard[1][2] ='Z';
    }else if(type == cell_block_type:: T){
        NextBoard[0][0] ='T';
        NextBoard[0][1] ='T';
        NextBoard[0][2] ='T';
        NextBoard[1][1] ='T';
    }
}


void TextDisplay::setModel(shared_ptr<QuadrisModel> model){
    quadris = model;
}


TextDisplay::TextDisplay(){
    
    for(int i = 0; i < row; i++){
        vector<char>temp;
        temp.clear();
        for(int j = 0; j < col; j++){
            temp.emplace_back(' ');
        }
        theDisplay.emplace_back(temp);
    }
    
    for(int i = 0; i < next_row; i++){
        vector<char>temp;
        temp.clear();
        for(int j = 0; j < next_col; j++){
            temp.emplace_back(' ');
        }
        NextBoard.emplace_back(temp);
    }
}


TextDisplay::~TextDisplay(){
}


void TextDisplay::notify(Subject& who){
    Info your_info = who.getInfo();
    
    if(your_info.type == cell_block_type::I){
        theDisplay[your_info.row][your_info.col] = 'I';
    }else if(your_info.type == cell_block_type::J){
        theDisplay[your_info.row][your_info.col] = 'J';
    }else if(your_info.type == cell_block_type::L){
        theDisplay[your_info.row][your_info.col] = 'L';
    }else if(your_info.type == cell_block_type::O){
        theDisplay[your_info.row][your_info.col] = 'O';
    }else if(your_info.type == cell_block_type::S){
        theDisplay[your_info.row][your_info.col] = 'S';
    }else if(your_info.type == cell_block_type::Z){
        theDisplay[your_info.row][your_info.col] = 'Z';
    }else if(your_info.type == cell_block_type::T){
        theDisplay[your_info.row][your_info.col] = 'T';
    }else if(your_info.type == cell_block_type::hint){
        theDisplay[your_info.row][your_info.col] = '?';
    }else if(your_info.type == cell_block_type::empty){
        theDisplay[your_info.row][your_info.col] = ' ';
    }else if(your_info.type == cell_block_type::ObyO){
        theDisplay[your_info.row][your_info.col] = '*';
    }
}



