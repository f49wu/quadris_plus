#include "AI_model_Displayer.h"
#include <iostream>

using namespace std;

void AI_displayer::setAI(shared_ptr<QuadrisModel>ai){
    AI = ai;
}


shared_ptr<Xwindow> AI_displayer::GetWindow() const{
    return win;
}


AI_displayer::AI_displayer(){
}


AI_displayer::~AI_displayer(){
}


void AI_displayer::init(){
    // create the window with the double width
    win = make_shared<Xwindow>(win_size_double, win_size);
    
    // initialize the frame of the grid
    win->fillRectangle(133 + AI_shift, win_size-670, 7, 650);
    win->fillRectangle(140 + AI_shift, win_size-27, 385, 7);
    win->fillRectangle(525 + AI_shift, win_size-670, 7, 650);
    
    win->drawString(540 + AI_shift, 30, "Next Block:");
    win->drawString(540 + AI_shift, 165, "Level:");
    string level_ai = to_string(AI->getLevel());
    win->drawString(630 + AI_shift, 165, level_ai);
    win->drawString(540 + AI_shift, 185, "Score:");
    string score_ai = to_string(AI->getScore());
    win->drawString(630 + AI_shift, 185, score_ai);
    win->drawString(540 + AI_shift, 205, "Hi Score:");
    string hi_score_ai = to_string(AI->getHiScore());
    win->drawString(630 + AI_shift, 205, hi_score_ai, Xwindow::Red);
    
    win->drawBigString(570 + AI_shift, 350, "A", Xwindow::Magenta);
    win->drawBigString(585 + AI_shift, 390, "I", Xwindow::Magenta);
    win->drawBigString(590 + AI_shift, 430, "M", Xwindow::Blue);
    win->drawBigString(600 + AI_shift, 470, "O", Xwindow::Blue);
    win->drawBigString(610 + AI_shift, 510, "D", Xwindow::Blue);
    win->drawBigString(620 + AI_shift, 550, "E", Xwindow::Blue);
    win->drawBigString(630 + AI_shift, 590, "L", Xwindow::Blue);
}


void AI_displayer::UpdateInfo(){
    // get the level, current score and hi_score from
    // the model and show up
    string level_ai = to_string(AI->getLevel());
    win->fillRectangle(630+AI_shift, 135, 50, 80, Xwindow::White);
    win->drawString(630+AI_shift, 165, level_ai);
    string score_ai = to_string(AI->getScore());
    win->drawString(630+AI_shift, 185, score_ai);
    string hi_score_ai = to_string(AI->getHiScore());
    win->drawString(630+AI_shift, 205, hi_score_ai);
}


void AI_displayer::notify(Subject& s){
    // show the cell who notified on the window
    
    Info your_info = s.getInfo();
    int row = your_info.row;
    int col = your_info.col;
    cell_block_type type = your_info.type;
    
    
    if(type == cell_block_type::I){
        win->fillRectangle(left_x + cell_size*col + margin + AI_shift, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Red);
    }else if(type == cell_block_type::J){
        win->fillRectangle(left_x + cell_size*col + margin + AI_shift, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Green);
    }else if(type == cell_block_type::L){
        win->fillRectangle(left_x + cell_size*col + margin + AI_shift, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Blue);
    }else if(type == cell_block_type::O){
        win->fillRectangle(left_x + cell_size*col + margin + AI_shift, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Cyan);
    }else if(type == cell_block_type::S){
        win->fillRectangle(left_x + cell_size*col + margin + AI_shift, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Yellow);
    }else if(type == cell_block_type::Z){
        win->fillRectangle(left_x + cell_size*col + margin + AI_shift, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Magenta);
    }else if(type == cell_block_type::T){
        win->fillRectangle(left_x + cell_size*col + margin + AI_shift, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Orange);
    }else if(type == cell_block_type::ObyO){
        win->fillRectangle(left_x + cell_size*col + margin + AI_shift, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Brown);
    }else if(type == cell_block_type::empty){
        win->fillRectangle(left_x + cell_size*col + margin + AI_shift, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::White);
    }
}


void AI_displayer::showNext(cell_block_type type){
    // show the next block on the window according to the given type
    
    win->fillRectangle(next_board_x + AI_shift, next_board_y, 140, 70, Xwindow::White);
    
    if(type == cell_block_type::I){
        win->fillRectangle(next_board_x + margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Red);
        win->fillRectangle(next_board_x + cell_size + margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Red);
        win->fillRectangle(next_board_x + 2*cell_size + margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Red);
        win->fillRectangle(next_board_x + 3*cell_size + margin + AI_shift, next_board_y +margin,
                           inner_size, inner_size, Xwindow::Red);
    }else if(type == cell_block_type::J){
        win->fillRectangle(next_board_x + margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Green);
        win->fillRectangle(next_board_x + margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Green);
        win->fillRectangle(next_board_x + cell_size + margin + AI_shift, next_board_y + cell_size+ margin,
                           inner_size, inner_size, Xwindow::Green);
        win->fillRectangle(next_board_x + 2*cell_size + margin + AI_shift, next_board_y + cell_size+ margin,
                           inner_size, inner_size, Xwindow::Green);
    }else if(type == cell_block_type::L){
        win->fillRectangle(next_board_x + margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Blue);
        win->fillRectangle(next_board_x + cell_size +margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Blue);
        win->fillRectangle(next_board_x + 2*cell_size + margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Blue);
        win->fillRectangle(next_board_x + 2*cell_size + margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Blue);
    }else if(type == cell_block_type::O){
        win->fillRectangle(next_board_x + margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Cyan);
        win->fillRectangle(next_board_x + cell_size +margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Cyan);
        win->fillRectangle(next_board_x + margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Cyan);
        win->fillRectangle(next_board_x + cell_size + margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Cyan);
    }else if(type == cell_block_type::S){
        win->fillRectangle(next_board_x + cell_size +margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Yellow);
        win->fillRectangle(next_board_x + 2*cell_size +margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Yellow);
        win->fillRectangle(next_board_x + margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Yellow);
        win->fillRectangle(next_board_x + cell_size + margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Yellow);
    }else if(type == cell_block_type::Z){
        win->fillRectangle(next_board_x +margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Magenta);
        win->fillRectangle(next_board_x + 1*cell_size +margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Magenta);
        win->fillRectangle(next_board_x + cell_size + margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Magenta);
        win->fillRectangle(next_board_x + 2*cell_size + margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Magenta);
    }else if(type == cell_block_type::T){
        win->fillRectangle(next_board_x +margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Orange);
        win->fillRectangle(next_board_x + 1*cell_size +margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Orange);
        win->fillRectangle(next_board_x + 2*cell_size + margin + AI_shift, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Orange);
        win->fillRectangle(next_board_x + cell_size + margin + AI_shift, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Orange);
    }
}


void AI_displayer::game_over(){
    win->fillRectangle(left_x + AI_shift, top_y, 385, 630, Xwindow::Blue);

    win->drawBigString(left_x + 170 + AI_shift, 150, "G", Xwindow::White);
     win->drawBigString(left_x + 170 + AI_shift, 200, "A",Xwindow::White);
     win->drawBigString(left_x + 170 + AI_shift, 250, "M",Xwindow::White);
     win->drawBigString(left_x + 170 + AI_shift, 300, "E",Xwindow::White);
     win->drawBigString(left_x + 170 + AI_shift, 400, "O",Xwindow::White);
     win->drawBigString(left_x + 170 + AI_shift, 450, "V",Xwindow::White);
     win->drawBigString(left_x + 170 + AI_shift, 500, "E",Xwindow::White);
     win->drawBigString(left_x + 170 + AI_shift, 550, "R",Xwindow::White);
}


