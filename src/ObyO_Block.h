#ifndef ObyO_Block_h
#define ObyO_Block_h

#include "block.h"

class Block;
class Cell;

class ObyO_Block: public Block{
    
    int row_num;
    
public:
    ObyO_Block(int level, cell_block_type type, int row_num);
    ~ObyO_Block();
    void clockwise(int) override;
    void counterclockwise(int) override;
    void notify(Subject& whoFrom) override;
};

#endif
