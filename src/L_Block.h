#ifndef L_Block_h
#define L_Block_h
#include "block.h"

class Block;
class Cell;

class L_Block: public Block{
    
    int final_rotate_shape(int time);
    
public:
    L_Block(int level, cell_block_type type);
    ~L_Block();
    void counterclockwise(int time) override;
    void clockwise(int time) override;
};

#endif
