#include "GraphicsDisplayer.h"

using namespace std;

void GraphicsDisplay::setAIdisplayer(shared_ptr<Xwindow> win){ 
    AI_model= true;
    this->win = win;
}


void GraphicsDisplay:: setModel(shared_ptr<QuadrisModel> model){
    this->model = model;
}


GraphicsDisplay::GraphicsDisplay(){
}


GraphicsDisplay::~GraphicsDisplay(){
}


shared_ptr<Xwindow> GraphicsDisplay::GetWindow() const{
    return nullptr;
}


void GraphicsDisplay::init(){
    
    if(!AI_model){
        win = make_shared<Xwindow>(win_size, win_size);
    }
    
    // setup the frame of the grid
    win->fillRectangle(133, win_size-670, 7, 650);
    win->fillRectangle(140, win_size-27, 385, 7);
    win->fillRectangle(525, win_size-670, 7, 650);
    
    int command_x = 25;
    
    int add = 30;
    
    // setup other fields
    win->drawString(command_x, 50 + add, "COMMANDS:", Xwindow::Blue);
    win->drawString(command_x, 70 + add, "-left",Xwindow::Blue);
    win->drawString(command_x, 90 + add, "-right",Xwindow::Blue);
    win->drawString(command_x, 110 + add, "-down",Xwindow::Blue);
    win->drawString(command_x, 130 + add, "-drop",Xwindow::Blue);
    win->drawString(command_x, 150 + add, "-clockwise",Xwindow::Blue);
    win->drawString(command_x, 170 + add, "-counterclockwise",Xwindow::Blue);
    win->drawString(command_x, 190 + add, "-levelup",Xwindow::Blue);
    win->drawString(command_x, 210 + add, "-nonrandom \"file\"",Xwindow::Blue);
    win->drawString(command_x, 230 + add, "-random",Xwindow::Blue);
    win->drawString(command_x, 250 + add, "-sequence \"file\"",Xwindow::Blue);
    win->drawString(command_x, 270 + add, "-I",Xwindow::Blue);
    win->drawString(command_x, 290 + add, "-J",Xwindow::Blue);
    win->drawString(command_x, 310 + add, "-L",Xwindow::Blue);
    win->drawString(command_x, 330 + add, "-O",Xwindow::Blue);
    win->drawString(command_x, 350 + add, "-S",Xwindow::Blue);
    win->drawString(command_x, 370 + add, "-Z",Xwindow::Blue);
    win->drawString(command_x, 390 + add, "-T",Xwindow::Blue);
    win->drawString(command_x, 410 + add, "-restart",Xwindow::Blue);
    win->drawString(command_x, 430 + add, "-hint",Xwindow::Blue);
    win->drawString(540, 30, "Next Block:");
    win->drawString(540, 165, "Level:");
    string level = to_string(model->getLevel());
    win->drawString(630, 165, level);
    win->drawString(540, 185, "Score:");
    string score = to_string(model->getScore());
    win->drawString(630, 185, score);
    win->drawString(540, 205, "Hi Score:");
    string hi_score = to_string(model->getHiScore());
    win->drawString(630, 205, hi_score, Xwindow::Red);
    
    win->drawBigString(570, 350, "Q", Xwindow::Magenta);
    win->drawBigString(580, 390, "U", Xwindow::Green);
    win->drawBigString(590, 430, "A", Xwindow::Blue);
    win->drawBigString(600, 470, "D", Xwindow::Red);
    win->drawBigString(610, 510, "R", Xwindow::Cyan);
    win->drawBigString(620, 550, "I", Xwindow::Blue);
    win->drawBigString(625, 590, "S", Xwindow::Orange);
}


void GraphicsDisplay::UpdateInfo(){
    // get the level, current score and hi_score from
    // the model and show up
    string level = to_string(model->getLevel());
    win->fillRectangle(630, 135, 50, 80, Xwindow::White);
    win->drawString(630, 165, level);
    string score = to_string(model->getScore());
    win->drawString(630, 185, score);
    string hi_score = to_string(model->getHiScore());
    win->drawString(630, 205, hi_score, Xwindow::Red);
}


void GraphicsDisplay::notify(Subject& s){
    
    // show the cell who notified on the window
    
    Info your_info = s.getInfo();
    
    int row = your_info.row;
    int col = your_info.col;
    cell_block_type type = your_info.type;
    
    
    if(type == cell_block_type::I){
        win->fillRectangle(left_x + cell_size*col + margin, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Red);
    }else if(type == cell_block_type::J){
        win->fillRectangle(left_x + cell_size*col + margin, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Green);
    }else if(type == cell_block_type::L){
        win->fillRectangle(left_x + cell_size*col + margin, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Blue);
    }else if(type == cell_block_type::O){
        win->fillRectangle(left_x + cell_size*col + margin, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Cyan);
    }else if(type == cell_block_type::S){
        win->fillRectangle(left_x + cell_size*col + margin, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Yellow);
    }else if(type == cell_block_type::Z){
        win->fillRectangle(left_x + cell_size*col + margin, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Magenta);
    }else if(type == cell_block_type::T){
        win->fillRectangle(left_x + cell_size*col + margin, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Orange);
    }else if(type == cell_block_type::ObyO){
        win->fillRectangle(left_x + cell_size*col + margin, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::Brown);
    }else if(type == cell_block_type::hint){
        win->drawString(left_x + cell_size*col + 20*margin, top_y + cell_size*row + 20*margin,
                        "?");
    }else if(type == cell_block_type::empty){
        win->fillRectangle(left_x + cell_size*col + margin, top_y + cell_size*row + margin,
                           cell_size-2*margin, cell_size-2*margin, Xwindow::White);
    }
}


void GraphicsDisplay::showNext(cell_block_type type){
    win->fillRectangle(next_board_x, next_board_y, 140, 70, Xwindow::White);
    
    
    // show the next block on the window according to the given type
    if(type == cell_block_type::I){
        win->fillRectangle(next_board_x + margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Red);
        win->fillRectangle(next_board_x + cell_size + margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Red);
        win->fillRectangle(next_board_x + 2*cell_size + margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Red);
        win->fillRectangle(next_board_x + 3*cell_size + margin, next_board_y +margin,
                           inner_size, inner_size, Xwindow::Red);
    }else if(type == cell_block_type::J){
        win->fillRectangle(next_board_x + margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Green);
        win->fillRectangle(next_board_x + margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Green);
        win->fillRectangle(next_board_x + cell_size + margin, next_board_y + cell_size+ margin,
                           inner_size, inner_size, Xwindow::Green);
        win->fillRectangle(next_board_x + 2*cell_size + margin, next_board_y + cell_size+ margin,
                           inner_size, inner_size, Xwindow::Green);
    }else if(type == cell_block_type::L){
        win->fillRectangle(next_board_x + margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Blue);
        win->fillRectangle(next_board_x + cell_size +margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Blue);
        win->fillRectangle(next_board_x + 2*cell_size + margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Blue);
        win->fillRectangle(next_board_x + 2*cell_size + margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Blue);
    }else if(type == cell_block_type::O){
        win->fillRectangle(next_board_x + margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Cyan);
        win->fillRectangle(next_board_x + cell_size +margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Cyan);
        win->fillRectangle(next_board_x + margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Cyan);
        win->fillRectangle(next_board_x + cell_size + margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Cyan);
    }else if(type == cell_block_type::S){
        win->fillRectangle(next_board_x + cell_size +margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Yellow);
        win->fillRectangle(next_board_x + 2*cell_size +margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Yellow);
        win->fillRectangle(next_board_x + margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Yellow);
        win->fillRectangle(next_board_x + cell_size + margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Yellow);
    }else if(type == cell_block_type::Z){
        win->fillRectangle(next_board_x +margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Magenta);
        win->fillRectangle(next_board_x + 1*cell_size +margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Magenta);
        win->fillRectangle(next_board_x + cell_size + margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Magenta);
        win->fillRectangle(next_board_x + 2*cell_size + margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Magenta);
    }else if(type == cell_block_type::T){
        win->fillRectangle(next_board_x +margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Orange);
        win->fillRectangle(next_board_x + 1*cell_size +margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Orange);
        win->fillRectangle(next_board_x + 2*cell_size + margin, next_board_y + margin,
                           inner_size, inner_size, Xwindow::Orange);
        win->fillRectangle(next_board_x + cell_size + margin, next_board_y + cell_size + margin,
                           inner_size, inner_size, Xwindow::Orange);
    }
}


void GraphicsDisplay::game_over(){
    
    win->fillRectangle(left_x, top_y, 385, 630, Xwindow::Magenta);
    
     win->drawBigString(left_x + 170, 150, "G",Xwindow::Black);
     win->drawBigString(left_x + 170, 200, "A",Xwindow::Black);
     win->drawBigString(left_x + 170, 250, "M",Xwindow::Black);
     win->drawBigString(left_x + 170, 300, "E",Xwindow::Black);
     win->drawBigString(left_x + 170, 400, "O",Xwindow::Black);
     win->drawBigString(left_x + 170, 450, "V",Xwindow::Black);
     win->drawBigString(left_x + 170, 500, "E",Xwindow::Black);
     win->drawBigString(left_x + 170, 550, "R",Xwindow::Black); 
}

