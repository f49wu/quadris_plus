#include "L_Block.h"
using namespace std;


int L_Block::final_rotate_shape(int time){
    int result = rotateShape + time%4;
    if(result > 4){
        result-=4;
    }
    return result;
}


L_Block::L_Block(int level, cell_block_type type):Block{level, type}{
    theBlock.emplace_back(Cell{4, 0, this->my_type});
    theBlock.emplace_back(Cell{4, 1, this->my_type});
    theBlock.emplace_back(Cell{4, 2, this->my_type});
    theBlock.emplace_back(Cell{3, 2, this->my_type});
}


L_Block::~L_Block(){
}


void L_Block::counterclockwise(int time){
    
    if(time%4 == 0){
        return;
    }
    
    int left_col = 0;
    int left_row = 0;
    
    if(rotateShape == 1){
        left_col = theBlock[0].getCol();
        left_row = theBlock[0].getRow();
    }else if(rotateShape == 2){
        left_col = theBlock[0].getCol();
        left_row = theBlock[3].getRow();
    }else if(rotateShape == 3){
        left_col = theBlock[0].getCol();
        left_row = theBlock[0].getRow();
    }else if(rotateShape == 4){
        left_col = theBlock[2].getCol();
        left_row = theBlock[2].getRow();
    }
    
    int final_shape = final_rotate_shape(time);
    
    if(final_shape == 1){
        
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.change_type(cell_block_type::empty);
        }
        
        theBlock[0].setRow(left_row);
        theBlock[0].setCol(left_col);
        theBlock[1].setRow(left_row);
        theBlock[1].setCol(left_col+1);
        theBlock[2].setRow(left_row);
        theBlock[2].setCol(left_col+2);
        theBlock[3].setRow(left_row-1);
        theBlock[3].setCol(left_col+2);
        
        
        for(auto&a: theBlock){
            a.change_type(this->my_type);
        }
        
        rotateShape = 1;
        
    }else if(final_shape == 2){
        
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.change_type(cell_block_type::empty);
        }
        
        theBlock[0].setRow(left_row-2);
        theBlock[0].setCol(left_col);
        theBlock[1].setRow(left_row-2);
        theBlock[1].setCol(left_col+1);
        theBlock[2].setRow(left_row-1);
        theBlock[2].setCol(left_col+1);
        theBlock[3].setRow(left_row);
        theBlock[3].setCol(left_col+1);
        
        
        for(auto&a: theBlock){
            a.change_type(this->my_type);
        }
        
        rotateShape = 2;
        
    }else if(final_shape == 3){
        
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.change_type(cell_block_type::empty);
        }
        
        theBlock[0].setRow(left_row);
        theBlock[0].setCol(left_col);
        theBlock[1].setRow(left_row-1);
        theBlock[1].setCol(left_col);
        theBlock[2].setRow(left_row-1);
        theBlock[2].setCol(left_col+1);
        theBlock[3].setRow(left_row-1);
        theBlock[3].setCol(left_col+2);
        
        
        for(auto&a: theBlock){
            a.change_type(this->my_type);
        }
        
        rotateShape = 3;
        
    }else if(final_shape == 4){
        
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.change_type(cell_block_type::empty);
        }
        
        theBlock[0].setRow(left_row-2);
        theBlock[0].setCol(left_col);
        theBlock[1].setRow(left_row-1);
        theBlock[1].setCol(left_col);
        theBlock[2].setRow(left_row);
        theBlock[2].setCol(left_col);
        theBlock[3].setRow(left_row);
        theBlock[3].setCol(left_col+1);
        
        
        for(auto&a: theBlock){
            a.change_type(this->my_type);
        }
        
        rotateShape = 4;
    }
}


void L_Block::clockwise(int time){
    counterclockwise(time*3);
}

