#ifndef Level_Zero_h
#define Level_Zero_h

#include "level.h"

class Block;
class Level;

class Level_Zero: public Level{
    // the current index of sequence(sequence of blocks)
    int index = 0;
    int length_sequence = 0;
    std::vector<std::string> sequence;
    cell_block_type _NextBlock() override;
    
public:
    Level_Zero(int level, std::string file_name = "sequence.txt");
    ~Level_Zero();
};


#endif
