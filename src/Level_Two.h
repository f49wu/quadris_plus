#ifndef Level_Two_h
#define Level_Two_h

#include "level.h"

class Block;
class Level;

class Level_Two: public Level{
    
    
    std::vector<cell_block_type> sequence;
    cell_block_type _NextBlock() override;
    
public:
    Level_Two(int level);
    ~Level_Two();
};

#endif
