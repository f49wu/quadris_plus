#ifndef Z_Block_h
#define Z_Block_h

#include "block.h"

class Block;
class Cell;

class Z_Block: public Block{
    
    
public:
    Z_Block(int level, cell_block_type type);
    ~Z_Block();
    void counterclockwise(int time) override;
    void clockwise(int time) override;
};
#endif
