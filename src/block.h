#ifndef block_h
#define block_h

#include "cell.h"

class Observer;
class Cell;
struct Info;
class Block: public Observer{
protected:
    std::vector <Cell> theBlock;
    cell_block_type my_type;
    
    // using an integer from 1 to 4
    // to represent the current rotate shape
    // (that is, the current block is being
    // rotated to which shape)
    int rotateShape;
    
    bool unmovable = false;
    bool is_delete = false;
    int cur_cells_num = 4;
    int level;
    bool is_heavy = false;
    
public:
    void setTheBlockPos(std::vector<Info> new_pos);
    bool is_unmovable() const;
    void setUnmoveable();
    virtual void notify(Subject& whoFrom) override;
    // empty() returns true when all its cells
    // has been cleared
    Block(int level, cell_block_type type);
    virtual ~Block() = 0;
    //Block_Position() return the info of position
    // of all its cells
    std::vector <Info> Block_Position();
    virtual void clockwise(int time) = 0;
    virtual void counterclockwise(int time) = 0;
    void left(int time);
    void right(int time);
    void down(int time);
    void setTextDisplayer(std::shared_ptr<Observer> td);
    void setGraphicsDisplayer(std::shared_ptr<Observer> dp);
    void drop();
    bool heavy() const;
    // disppear()makes the block disappear on the board
    void disappear();
    void appear();
    void setRotateShape(int new_shape);
    int GetRotateShape() const;
    int getLevel() const;
    cell_block_type getBlockType() const;
    bool empty() const;
};
#endif
