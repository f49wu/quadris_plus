#include "Quadris_Controller.h"
#include <ctime>
#include <fstream>
#include <iostream>

using namespace std;

class AI_displayer;

int main(int argc, char* argv[]) {
    
    
    bool text_model = false;
    bool ai_model = false;
    int seed = (int)time(0);
    string scriptdfile = "sequence.txt";
    int start_level = 0;
    
    // extra all falgs
    for(int i = 0; i < argc; i++){
        
        string cur_flag{argv[i]};
        if(cur_flag == "-text"){
            text_model = true;
        }
        if(cur_flag == "-seed"){
            seed = stoi(argv[i+1]);
        }
        if(cur_flag == "-scriptfile"){
            scriptdfile = argv[i+1];
            // to check if the scriptfile exists
            fstream fs_check;
            fs_check.open(scriptdfile);
            if(!fs_check.is_open()){
                cout<<"ERROR: can not open scriptfile: "
                <<scriptdfile<<endl;
                scriptdfile = "sequence.txt";
            }
            fs_check.close();
        }
        if(cur_flag == "-startlevel"){
            cout<<"setting start level"<<endl;
            string set_level{argv[i+1]};
            start_level = stoi(set_level);
        }
        if(cur_flag == "-AI"){
            ai_model = true;
        }
    }
    
    if(!ai_model){
        std::shared_ptr<TextDisplay> td = make_shared<TextDisplay>();
        std::shared_ptr<QuadrisModel> model = make_shared<QuadrisModel>();
        std::shared_ptr<GraphicsDisplay> gd = make_shared<GraphicsDisplay>();
        
        td->setModel(model);
        gd->setModel(model);
        Quadris_Controller theGame{gd, model, td, text_model, seed, scriptdfile, start_level};
        
        theGame.start();
        
    }else{
        // setup the AI model
        std::shared_ptr<TextDisplay> td_user = make_shared<TextDisplay>();
        std::shared_ptr<TextDisplay> td_ai = make_shared<TextDisplay>();
        
        std::shared_ptr<QuadrisModel> model_user = make_shared<QuadrisModel>();
        std::shared_ptr<QuadrisModel> model_AI = make_shared<QuadrisModel>();
        
        std::shared_ptr<GraphicsDisplay> gd_user = make_shared<GraphicsDisplay>();
        std::shared_ptr<AI_displayer> gd_AI = make_shared<AI_displayer>();
        
        td_user->setModel(model_user);
        td_ai->setModel(model_AI);
        
        gd_user->setModel(model_user);
        gd_AI->setAI(model_AI);
        
        model_AI->init(td_ai, gd_AI, text_model, seed, scriptdfile, start_level);
        gd_user->setAIdisplayer(gd_AI->GetWindow());
        Quadris_Controller theGame{gd_user, model_user, td_user, text_model, seed, scriptdfile, start_level};
        theGame.setAI(model_AI);
        
        theGame.start();
    }
}
