#ifndef QuadrisModel_h
#define QuadrisModel_h

#include "grid.h"
#include "level.h"
#include "Level_Zero.h"
#include "Level_One.h"
#include "Level_Two.h"
#include "Level_Three.h"
#include "I_Block.h"
#include "J_Block.h"
#include "S_Block.h"
#include "L_Block.h"
#include "O_Block.h"
#include "Z_Block.h"
#include "T_Block.h"
#include "ObyO_Block.h"
#include <memory>

class Grid;
class Block;
class GraphicsDisplay;
class AI_displayer;

class QuadrisModel{
    const int row = 18;
    const int col = 11;
    const int first_row = 3;
    
    std::shared_ptr<TextDisplay> td;
    std::shared_ptr<GraphicsDisplay>gd;
    bool text_model = false;
    bool is_game_over = false;
    int level;
    int start_level;
    int seed;
    int hi_score;
    int cur_score;
    int level_of_next_block;
    std::unique_ptr<Grid> g;
    std::shared_ptr<Block>cur_block;
    cell_block_type next_block;
    std::unique_ptr<Block> hint_block;
    std::unique_ptr<Level> cur_level;
    std::string file_name = "sequence.txt";
    
    // call clear_rows() in grid to update cur_score
    void clear_rows();
    
    //stores all blocks on the grid
    std::vector<std::shared_ptr<Block>> blocks_on_Grid;
    
    // current suggested position
    std::vector<Info> cur_hint_pos;
    bool hint_on_board = false;
    void GenerateOne_by_OneBlock();
    
    // for level 4, counter the number of dropped block
    // without clearing a row
    int num_without_clear = 0;
    
    void GenerateHintBlock();
    void update_socre(int adding_score);
    int sqrt(int num);
    
    // determine if a move is valid
    bool canMove(std::vector<Info> test_positions);
    
    // let the current block be the next one
    // show next block
    void NextBlock();
    
    // 4 move function to move a virtual block (used for canMove())
    void positionUp(std::vector<Info> &test_positions, int m);
    void positionDown(std::vector<Info> &test_positions, int m);
    void positionRight(std::vector<Info> &test_positions, int m);
    void positionLeft(std::vector<Info> &test_positions, int m);
    
    bool isOver();
    void GameOver();
    // move current block downward automatically
    void autodown();
    // return the score of a posible move(used for hint)
    double Move_score(std::vector<std::vector<Info>>& theGrid);
    
    // three helper functions used for hint
    int getCompleteLine(std::vector<std::vector<Info>>& theGrid);
    int getNum_Holes(std::vector<std::vector<Info>>& theGrid);
    int getAggregateHeight(std::vector<std::vector<Info>>& theGrid,
                           std::vector<int>& heights);
    
    int abs(int a); // absolute value
    void setHiScore(int hi_score);
    // change the Next block according to the current level
    void changeNext();
    void clearHint();
    
public:
    void nonRandom(std::string file);
    void Random();
    void restart();
    ~QuadrisModel();
    void swap_cur_Block(cell_block_type new_type);
    void init(std::shared_ptr<TextDisplay>td, std::shared_ptr<GraphicsDisplay> gd, bool text_model = false, int seed =  0,
              std::string source_file = "sequence.txt", int level = 0);
    void down(int m);
    void right(int m);
    void left(int m);
    void drop(int m);
    void clockwise(int m);
    void cclockwise(int m);
    void levelUp(int m);
    void levelDown(int m);
    void setSeqFile(std::string);
    int getHiScore() const;
    int getLevel() const;
    int getScore() const;
    std::shared_ptr<TextDisplay> getTextDisplay() const;
    std::shared_ptr<GraphicsDisplay> getGraphicsDisplay() const;

    // show the hint on the board
    void getHint();
    // drop a block automatically at the position
    // suggested by the hint()
    void AI_auto_step();
    // check if the game is over
    bool game_over() const;
};

#endif
