#include "block.h"

using namespace std;

void Block::setRotateShape(int new_shape){
    rotateShape = new_shape;
}


int Block::GetRotateShape() const{
    return rotateShape;
}


void Block::setTheBlockPos(vector<Info> new_pos){
    int i = 0;
    for(auto& a: new_pos){
        theBlock[i].setCol(a.col);
        theBlock[i].setRow(a.row);
        i++;
    }
}


void Block::setTextDisplayer(std::shared_ptr<Observer> td){
    for(auto &a: theBlock){
        a.attach(td);
        a.notifyObservers();
    }
}


void Block::setGraphicsDisplayer(std::shared_ptr<Observer> gd){
    for(auto &a: theBlock){
        a.attach(gd);
        a.notifyObservers();
    }
}


void Block::setUnmoveable(){
    unmovable = true;
}


bool Block::is_unmovable()const{
    return unmovable;
}


bool Block::empty() const{
    return is_delete;
}


void Block::notify(Subject& whoFrom){
    
    if(whoFrom.getState() == StateType::block_cell_cleared){
        cur_cells_num--;
    }
    
    if(cur_cells_num <= 0){
        is_delete = true;
    }
}


bool Block::heavy() const{
    return is_heavy;
}


Block::~Block(){
}


void Block::left(int time){
    for(auto& c: theBlock){
        c.change_type(cell_block_type::empty);
        c.left(time);
    }
    for(auto& c: theBlock){
        c.change_type(this->my_type);
    }
}


void Block::right(int time){
    for(auto& c: theBlock){
        c.change_type(cell_block_type::empty);
        c.right(time);
    }
    for(auto& c: theBlock){
        c.change_type(this->my_type);
    }
}


void Block::down(int time){
    for(auto& c: theBlock){
        c.change_type(cell_block_type::empty);
        c.down(time);
    }
    
    for(auto& c: theBlock){
        c.change_type(this->my_type);
    }
}


void Block::disappear(){
    for(auto& a: theBlock){
        a.setState(StateType::no_state);
        a.change_type(cell_block_type::empty);
    }
}


void Block::appear(){
    for(auto& a: theBlock){
        a.setState(StateType::no_state);
        a.change_type(this->my_type);
    }
}


vector<Info> Block::Block_Position(){
    
    // returns vector<info> containing all
    // position infomation of its cell
    
    vector<Info> result;
    result.clear();
    
    for(auto& a: theBlock){
        result.emplace_back(a.getInfo());
    }
    
    return result;
}


cell_block_type Block::getBlockType() const{
    return my_type;
}


int Block::getLevel() const{
    return level;
}


Block::Block(int level, cell_block_type type):my_type{type}, rotateShape{1}, level{level}{
    if(level >= 3){
        is_heavy = true;
    }
}

// make the block disappear, since it will be showed up
// on the grid
void Block::drop(){
    this->disappear();
}

