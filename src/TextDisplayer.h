#ifndef TextDisplayer_h
#define TextDisplayer_h

#include "subject.h"
#include "QuadrisModel.h"
#include <iostream>
#include <memory>

class Cell;
class QuadrisModel;
class Observer;
class Subject;

class TextDisplay: public Observer {
    std::shared_ptr<QuadrisModel> quadris;
    std::vector<std::vector<char>> theDisplay;
    // NextBoard stores the 2*4 char grid for
    // displaying the next block
    std::vector<std::vector<char>> NextBoard;
    const int row = 18;
    const int col = 11;
    const int next_row = 2;
    const int next_col = 4;
    
public:
    // show game_over on Textdisplayer
    void game_over();
    // clear() clears theDisplay and NextBoard
    void clear();
    TextDisplay();
    ~TextDisplay();
    // ShowNextBlock(next_type) shows the next block
    // according to the given cell_block_type
    void ShowNextBlock(cell_block_type next_type);
    void notify(Subject &whoNotified) override;
    void setModel(std::shared_ptr<QuadrisModel> model);
    friend std::ostream &operator<<(std::ostream &out, const TextDisplay &td);
    
};

std::ostream &operator<<(std::ostream &out, const TextDisplay &td);

#endif
