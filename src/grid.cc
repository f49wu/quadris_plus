#include "grid.h"

using namespace std;

void Grid::hint(std::vector<Info> pos){
    for(auto&a: pos){
        theGrid[a.row][a.col].change_type
        (cell_block_type::hint);
    }
}


void Grid::clear_hint(std::vector<Info> pos){ 
    for(auto&a: pos){
        theGrid[a.row][a.col].change_type
        (cell_block_type::empty);
    }
}


vector<vector<Info>> Grid::GetTheGrid(){
    vector<vector<Info>> result;
    
    for(int i = 0; i < row; i++){
        vector<Info> temp;
        temp.clear();
        for(int j = 0; j < col; j++){
            temp.emplace_back(theGrid[i][j].getInfo());
        }
        result.emplace_back(temp);
    }
    
    return result;
}


bool Grid::is_empty_cell(int r, int c) const{
    
    Info your_info = theGrid[r][c].getInfo();
    
    if(your_info.type == cell_block_type::empty){
        return true;
    }
    
    return false;
}


int Grid::clear_row(){
    
    int row_cleared = 0;
    for(int i = row-1; i >= top_row; i--){
        if(is_full(i)){
            row_cleared++;
            for(int j = 0; j < col; j++){
                // notify the block firstly
                theGrid[i][j].setState(StateType::block_cell_cleared);
                theGrid[i][j].notifyObservers();
                // then detach the block (block is its observer)
                theGrid[i][j].detach();
                // notify all displayers
                theGrid[i][j].setState(StateType::cleared);
                theGrid[i][j].change_type(cell_block_type::empty);
            }
            this->move_down(i);
            i+=1;
        }
    }
    
    return row_cleared;
}


bool Grid::is_full(int row){
    for(int i = 0; i < col; i++){
        Info your_info = theGrid[row][i].getInfo();
        if(your_info.type == cell_block_type::empty){
            return false;
        }
    }
    return true;
}


void Grid::setNewBlock(shared_ptr<Block> b){
    cell_block_type type = b->getBlockType();
    vector<Info> positions = b->Block_Position();
    
    for(auto a:  positions){
        int row = a.row;
        int col = a.col;
        
        theGrid[row][col].setState(StateType::no_state);
        theGrid[row][col].change_type(type);
        // attach the block to the cell as an observer
        theGrid[row][col].attach(b);
    }
    
}


Grid::Grid(){
}


Grid::~Grid(){
}


void Grid::init(){
    
    for(int i = 0; i < row; i++){
        vector<Cell> temp;
        temp.clear();
        for(int j = 0; j < col; j++){
            temp.emplace_back
            (Cell{i,j,cell_block_type::empty});
        }
        theGrid.emplace_back(temp);
    }
}


void Grid::move_down(int r_cleared){
    for(int i = 0; i < col; i++){
        for(int j = r_cleared; j >= top_row; j--){
            if(j == top_row){
                theGrid[j][i].change_type
                (cell_block_type::empty);
            }else{
                Info your_info = theGrid[j-1][i].getInfo();
                Info my_info = theGrid[j][i].getInfo();
                if(your_info.type == cell_block_type::empty){
                    if(my_info.type == cell_block_type::empty){
                        continue;
                    }
                    theGrid[j][i].change_type
                    (cell_block_type::empty);
                }else{
                    theGrid[j][i].attach
                    (theGrid[j-1][i].detach());
                    theGrid[j][i].change_type(your_info.type);
                }
            }
        }
    }
}


void Grid::setTextDispalyer(std::shared_ptr<TextDisplay> td){
    
    for(auto&row: theGrid){
        for(auto&b: row){
            b.attach(td);
        }
    }
}


void Grid::setGraphicsDisplayer(std::shared_ptr<GraphicsDisplay> gd){ 
    
    for(auto&row: theGrid){
        for(auto&b: row){
            b.attach(gd);
        }
    }
}
