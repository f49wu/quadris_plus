#include "Level_Three.h"
#include <fstream>
#include <iostream>

using namespace std;

Level_Three::Level_Three(int level):Level{level}{
    sequence.emplace_back(cell_block_type::S);
    sequence.emplace_back(cell_block_type::Z);
    sequence.emplace_back(cell_block_type::L);
    sequence.emplace_back(cell_block_type::O);
    sequence.emplace_back(cell_block_type::I);
    sequence.emplace_back(cell_block_type::J);
    sequence.emplace_back(cell_block_type::T);
}


Level_Three::~Level_Three(){
}


void Level_Three::_setRandom(){
    is_random = true;
}


void Level_Three::_setNoRandom(string file){
    NoRandomfile = file;
    fstream fs;
    string ss;
    
    fs.open(file);
    
    // if the file does not exist
    // output the error message
    if(!fs.is_open()){
        cout<<"set noramdom error: no file"<<endl;
        return;
    }
    
    is_random = false;
    int i = 0;
    
    NoRandomSequence.clear();
    
    while(fs>>ss){
        NoRandomSequence.emplace_back(ss);
        ss.clear();
        i++;
    }
    
    len_NoRandom = i;
    index = 0;
    fs.close();
}


cell_block_type Level_Three::_NextBlock(){
    
    if(is_random){
        int raw_random = rand();
        // get random number from 0 to 8
        int random_num = raw_random%9;
        
        // return the type of next block according
        // to the random number
        if(random_num == 0 || random_num == 1){
            return sequence[0];
        }else if(random_num == 2 || random_num == 3){
            return sequence[1];
        }else if (random_num > 3 && random_num < 9){
            return sequence[random_num-2];
        }else{
            return cell_block_type::empty;
        }
        
    }else{
        // norandom model
        if(index>= len_NoRandom){
            index = 0;
        }
        
        if(NoRandomSequence[index] == "I"){
            index++;
            return cell_block_type::I;
        }else if(NoRandomSequence[index] == "J"){
            index++;
            return cell_block_type::J;
        }else if(NoRandomSequence[index] == "O"){
            index++;
            return cell_block_type::O;
        }else if(NoRandomSequence[index] == "S"){
            index++;
            return cell_block_type::S;
        }else if(NoRandomSequence[index] == "Z"){
            index++;
            return cell_block_type::Z;
        }else if(NoRandomSequence[index] == "T"){
            index++;
            return cell_block_type::T;
        }else if(NoRandomSequence[index] == "L"){
            index++;
            return cell_block_type::L;
        }
        return cell_block_type::empty;
    }
}
