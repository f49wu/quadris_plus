#ifndef _SUBJECT_H_
#define _SUBJECT_H_

#include <vector>
#include <memory>
#include "observer.h"
#include "Info.h"

struct Info;
class State;

class Observer;

class Subject {
    std::vector<std::shared_ptr<Observer>> observers;
    StateType my_state;
public:
    void attach(std::shared_ptr<Observer> o);
    std::shared_ptr<Observer> detach();
    void notifyObservers();
    virtual Info getInfo() const = 0;
    StateType getState() const;
    void setState(StateType new_state);
    virtual ~Subject();
};

#endif
