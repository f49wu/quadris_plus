#ifndef Level_h
#define Level_h

#include <string>
#include "Info.h"
#include <vector>

class Block;

class Level{
    int level;
    
protected:
    // is_random determine if it's random model
    // of norandom model
    bool is_random = true;
    
    int seed = 0;
    std::string sequence_file;
    virtual cell_block_type _NextBlock() = 0;
    virtual void _setRandom();
    virtual void _setNoRandom(std::string file);
public:

    // using virtual NextBlock() to generate blocks
    // according to current level
    cell_block_type NextBlock();
    
    Level(int level, std::string file_name = "sequence.txt");
    virtual ~Level() = 0;
    void setSequence(std::string file_name);
    void SetSeed(int seed);
    bool is_in_random();
    void setRandom();
    void setNoRandom(std::string file);
};

#endif
