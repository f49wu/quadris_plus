#include "Level_One.h"

Level_One::Level_One(int level):Level{level}{
    sequence.emplace_back(cell_block_type::S);
    sequence.emplace_back(cell_block_type::Z);
    sequence.emplace_back(cell_block_type::L);
    sequence.emplace_back(cell_block_type::O);
    sequence.emplace_back(cell_block_type::I);
    sequence.emplace_back(cell_block_type::J);
    sequence.emplace_back(cell_block_type::T);
}


Level_One::~Level_One(){
}


cell_block_type Level_One::_NextBlock(){
    // get a random number from 0 to 11
    int random_num = rand()%12;
    
    // return the type of next block according
    // to the random number
    if(random_num == 0){
        return sequence[0];
    }else if(random_num == 1){
        return sequence[1];
    }else if(random_num == 2 || random_num == 3){
        return sequence[2];
    }else if(random_num == 4 || random_num == 5){
        return sequence[3];
    }else if(random_num == 6 || random_num == 7){
        return sequence[4];
    }else if(random_num == 8 || random_num == 9){
        return sequence[5];
    }else if(random_num == 10 || random_num == 11){
        return sequence[6];
    }
    return sequence[0];
    
}
