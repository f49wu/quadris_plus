#include "level.h"
#include <cstdlib>
#include <fstream>
#include <iostream>

using namespace std;

void Level::setSequence(std::string file_name){
    sequence_file = file_name;
}


void Level::SetSeed(int seed){
    this->seed = seed;
    srand(seed);
}


void Level::setNoRandom(string f){
    //check if the file f exists
    fstream fs;
    
    fs.open(f);
    
    if(!fs.is_open()){
        cout<<"ERROR: can not open file: "
        <<f<<endl;
        return;
    }
    
    fs.close();
    
    this->_setNoRandom(f);
}


void Level::_setRandom(){
}


void Level::_setNoRandom(string f){
    (void)f;
}


bool Level::is_in_random(){
    return is_random;
}


void Level::setRandom(){
    if(!is_random){
        this->_setRandom();
    }
}


cell_block_type Level::NextBlock(){
    return this->_NextBlock();
}


Level::Level(int level, string file_name):level{level}, sequence_file{file_name}{
}


Level::~Level(){
}



