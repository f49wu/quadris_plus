#include "O_Block.h"

using namespace std;


O_Block::O_Block(int level, cell_block_type type):Block{level, type}{
    theBlock.emplace_back(Cell{3, 0, this->my_type});
    theBlock.emplace_back(Cell{4, 0, this->my_type});
    theBlock.emplace_back(Cell{4, 1, this->my_type});
    theBlock.emplace_back(Cell{3, 1, this->my_type});
}

O_Block::~O_Block(){
}

void O_Block::counterclockwise(int time){
    (void)time;
}

void O_Block::clockwise(int time){
    (void)time;
}
