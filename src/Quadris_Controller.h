#ifndef Quadris_Controller_h
#define Quadris_Controller_h

#include "QuadrisModel.h"
#include "AI_model_Displayer.h"
#include <iostream>

class Quadris_Controller{
    std::shared_ptr<QuadrisModel> model;
    std::shared_ptr<QuadrisModel> AI_model;
    
    // if adding the new feature
    bool ai_model = false;
    
    // match the given string to a command
    void cmdMatch(std::string cmd);
    
    // read command from file
    void readfile(std::string file_name);
public:
    Quadris_Controller();
    Quadris_Controller(std::shared_ptr<GraphicsDisplay> gd,
                       std::shared_ptr<QuadrisModel> p, std::shared_ptr<TextDisplay> td,
                       bool textmodel = false, int seed = 0,
                       std::string sourcefile = "sequence.txt",
                       int startlevel = 0);
    // setup AI model for the new feature
    void setAI(std::shared_ptr<QuadrisModel> ai);
    void start();
};


#endif
