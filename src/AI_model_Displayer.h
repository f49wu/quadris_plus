#ifndef AI_model_Displayer_h
#define AI_model_Displayer_h

#include "subject.h"
#include "QuadrisModel.h"
#include "window.h"

class QuadrisModel;

class AI_displayer: public GraphicsDisplay{
    std::shared_ptr<QuadrisModel> AI;
    std::shared_ptr<Xwindow> win;

    // shift the same game board of normal model to
    // the right to show the AI's game board
    const int AI_shift = 600;
    
    const int win_size_double = 1400;
    
public:
    // return the Xwindow win
    std::shared_ptr<Xwindow> GetWindow()const override;
    
    AI_displayer();
    ~AI_displayer();
    
    // setup the QuadrisModel for AI
    void setAI(std::shared_ptr<QuadrisModel> ai);
    
    // 4 overrided function for AI_model
    void UpdateInfo() override;
    void init() override;
    void notify(Subject &whoFrom) override;
    void game_over() override;
    void showNext(cell_block_type type) override;
};

#endif

