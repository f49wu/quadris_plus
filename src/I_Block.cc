#include "I_Block.h"

using namespace std;

I_Block::I_Block(int level, cell_block_type type):Block{level, type}{
    theBlock.emplace_back(Cell{3, 0, this->my_type});
    theBlock.emplace_back(Cell{3, 1, this->my_type});
    theBlock.emplace_back(Cell{3, 2, this->my_type});
    theBlock.emplace_back(Cell{3, 3, this->my_type});
}

I_Block::~I_Block(){
}


void I_Block::counterclockwise(int time){
    
    if(time%2 == 0){
        return;
    }
    
    if(rotateShape == 1){
        int left_col = theBlock[0].getCol();
        int row = theBlock[0].getRow();
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.change_type(cell_block_type::empty);
        }
        int i = 0;
        for(auto&a : theBlock){
            a.setCol(left_col);
            a.setRow(row - i);
            a.change_type(this->my_type);
            i++;
        }
        
        rotateShape = 2;
        
    }else if(rotateShape == 2){
        int left_row = theBlock[0].getRow();
        int col = theBlock[0].getCol();
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.change_type(cell_block_type::empty);
        }
        int i = 0;
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.setRow(left_row);
            a.setCol(col + i);
            a.change_type(this->my_type);
            i++;
        }
        
        rotateShape = 1;
    }
}


void I_Block::clockwise(int time){
    counterclockwise(time);
}
