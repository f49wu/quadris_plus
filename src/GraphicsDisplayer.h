#ifndef GraphicsDisplayer_h
#define GraphicsDisplayer_h

#include "subject.h"
#include "QuadrisModel.h"
#include "window.h"
#include <memory>

class QuadrisModel;
class GraphicsDisplay: public Observer{
protected:
    std::shared_ptr<Xwindow> win;
    std::shared_ptr<QuadrisModel> model;
    
    const int win_size = 700;
    const int cell_size = 35;
    
    const int left_x = 140;
    const int right_x = 525;
    
    const int top_y = 43;
    const int bottom_y = 673;
    
    const int margin = 1;
    const int inner_size = 33;
    
    const int next_board_x = 540;
    const int next_board_y = 45;
    
    bool AI_model = false;
    
public:
    GraphicsDisplay();
    ~GraphicsDisplay();
    void setAIdisplayer(std::shared_ptr<Xwindow> win);
    void setModel(std::shared_ptr<QuadrisModel> model);
    virtual void UpdateInfo();
    virtual void init();
    virtual void notify(Subject &whoFrom) override;
    // show game over on the window
    virtual void game_over();
    // show next block on the window
    virtual void showNext(cell_block_type type);
    // return the Xwindow win
    virtual std::shared_ptr<Xwindow> GetWindow()const;
};

#endif
