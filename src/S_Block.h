#ifndef S_Block_h
#define S_Block_h

#include "block.h"

class Block;
class Cell;

class S_Block: public Block{
    
    
public:
    S_Block(int level, cell_block_type type);
    ~S_Block();
    void counterclockwise(int time) override;
    void clockwise(int time) override;
};


#endif 
