#ifndef J_Block_h
#define J_Block_h
#include "block.h"

class Block;
class Cell;

class J_Block: public Block{
    
    int final_rotate_shape(int time);
    
public:
    J_Block(int level, cell_block_type type);
    ~J_Block();
    void counterclockwise(int time) override;
    void clockwise(int time) override;
};

#endif
