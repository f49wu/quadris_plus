#ifndef _Info_h
#define _Info_h

enum class cell_block_type {I, J, L, O, S, Z, T, empty, hint, ObyO};

// cleared means the cell has been cleared from the board,
// block_cell_cleared is to notify the block that
// the cell of that block has been cleared
enum class StateType{no_state, cleared, block_cell_cleared};

struct Info{
    int row;
    int col;
    cell_block_type type = cell_block_type::empty;
};

#endif
