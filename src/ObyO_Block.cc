#include "ObyO_Block.h"

ObyO_Block::ObyO_Block(int level, cell_block_type type, int row_num):Block{level, type}, row_num{row_num}{ 
    theBlock.emplace_back(Cell{row_num, 5, this->my_type});
}


ObyO_Block::~ObyO_Block(){
}


void ObyO_Block::clockwise(int){
}


void ObyO_Block::counterclockwise(int){ 
}


void ObyO_Block::notify(Subject& whoFrom){
    if(whoFrom.getState() == StateType::cleared){
        is_delete = true;
    }
}

