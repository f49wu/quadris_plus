#include "cell.h"

using namespace std;

void Cell::setCol(int new_col){
    col = new_col;
}


void Cell::setRow(int new_row){
    row = new_row;
}


int Cell::getCol() const{
    return col;
}


int Cell::getRow() const{
    return row;
}


void Cell::right(int time){
    col+=time;
}


void Cell::left(int time){
    col-=time;
}


void Cell::down(int time){
    row+=time;
}


Info Cell::getInfo() const{
    Info result = {row, col, type};
    return result;
}


void Cell::change_type(cell_block_type new_type){
    type = new_type;
    notifyObservers();
}


Cell::Cell(int row, int col, cell_block_type type):row{row}, col{col}, type{type}{
    setState(StateType::no_state);
}


Cell::~Cell(){
}

