#ifndef T_Block_h
#define T_Block_h

#include "block.h"

class Block;
class Cell;

class T_Block: public Block{
    
    int final_rotate_shape(int time);
    
public:
    T_Block(int level, cell_block_type type);
    ~T_Block();
    void counterclockwise(int time) override;
    void clockwise(int time) override;
};

#endif
