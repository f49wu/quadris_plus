#include "Level_Zero.h"
#include <iostream>
#include <fstream>

using namespace std;

Level_Zero::Level_Zero(int level, string file_name):Level{level, file_name}{
    
    fstream fs;
    
    string ss;
    
    fs.open(file_name);
    
    if(!fs.is_open()){
        cout<<"ERROR: NO SOURCEFILE"<<endl;
        return;
    }
        
    int i = 0;
        
    while(fs>>ss){
        sequence.emplace_back(ss);
        ss.clear();
        i++;
    }
        
    length_sequence = i;
        
    fs.close();
}
        
        
Level_Zero::~Level_Zero(){
}
        
        
cell_block_type Level_Zero::_NextBlock(){
            
    if(index>= length_sequence){
        index = 0;
    }
            
    if(sequence[index] == "I"){
        index++;
        return cell_block_type::I;
    }else if(sequence[index] == "J"){
        index++;
        return cell_block_type::J;
    }else if(sequence[index] == "O"){
        index++;
        return cell_block_type::O;
    }else if(sequence[index] == "S"){
        index++;
        return cell_block_type::S;
    }else if(sequence[index] == "Z"){
        index++;
        return cell_block_type::Z;
    }else if(sequence[index] == "T"){
        index++;
                return cell_block_type::T;
    }else if(sequence[index] == "L"){
        index++;
        return cell_block_type::L;
    }
    
    // this code will never be executed
    return cell_block_type::empty;
}
        
        
        
