#include "Level_Two.h"
#include <cstdlib>

Level_Two::Level_Two(int level):Level(level){ 
    sequence.emplace_back(cell_block_type::I);
    sequence.emplace_back(cell_block_type::J);
    sequence.emplace_back(cell_block_type::L);
    sequence.emplace_back(cell_block_type::O);
    sequence.emplace_back(cell_block_type::S);
    sequence.emplace_back(cell_block_type::Z);
    sequence.emplace_back(cell_block_type::T);
}


Level_Two::~Level_Two(){
}


cell_block_type Level_Two::_NextBlock(){
    // get a random number from 0 to 6
    int random_index = rand()%7;
    
    // return the type of next block according
    // to the random number
    return sequence[random_index];
}
