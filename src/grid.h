#ifndef grid_h
#define grid_h

#include "TextDisplayer.h"
#include "GraphicsDisplayer.h"

class Cell;
class Block;
class TextDisplay;
class GraphicsDisplay;

class Grid{
    std::vector<std::vector<Cell>> theGrid;
    
    // is_full(row) returns true if the row is full
    // returns false otherwise
    bool is_full(int row);
    
    const int row = 18;
    const int col = 11;
    const int top_row = 3;
    
public:
    // GetTheGrid()return a two-dimension vector of Info
    // that represents all cell on the grid
    std::vector<std::vector<Info>> GetTheGrid();
    // hint(pos) set type of the cell with position in pos
    // to hint
    void hint(std::vector<Info> pos);
    void clear_hint(std::vector<Info> pos);
    Grid();
    ~Grid();
    void init();
    bool is_empty_cell(int row, int col) const;
    // move_down(row_cleared) moves the grid that above thr row
    // being cleared downwards
    void move_down(int row_cleared);
    void setTextDispalyer(std::shared_ptr<TextDisplay> td);
    void setGraphicsDisplayer(std::shared_ptr<GraphicsDisplay> gd);
    void setNewBlock(std::shared_ptr<Block> b);
    int clear_row();
};

#endif
