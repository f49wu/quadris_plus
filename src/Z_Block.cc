#include "Z_Block.h"

using namespace std;


Z_Block::Z_Block(int level, cell_block_type type):Block{level, type}{
    theBlock.emplace_back(Cell{3, 0, this->my_type});
    theBlock.emplace_back(Cell{3, 1, this->my_type});
    theBlock.emplace_back(Cell{4, 1, this->my_type});
    theBlock.emplace_back(Cell{4, 2, this->my_type});
}


Z_Block::~Z_Block(){
}


void Z_Block::counterclockwise(int time){
    if(time%2 == 0){
        return;
    }
    
    if(rotateShape == 1){
        int left_col = theBlock[0].getCol();
        int left_row = theBlock[3].getRow();
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.change_type(cell_block_type::empty);
        }
        
        theBlock[0].setRow(left_row);
        theBlock[0].setCol(left_col);
        theBlock[1].setRow(left_row-1);
        theBlock[1].setCol(left_col);
        theBlock[2].setRow(left_row-1);
        theBlock[2].setCol(left_col+1);
        theBlock[3].setRow(left_row-2);
        theBlock[3].setCol(left_col+1);
        
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.change_type(this->my_type);
        }
        
        
        rotateShape = 2;
        
    }else if(rotateShape == 2){
        int left_col = theBlock[0].getCol();
        int left_row = theBlock[0].getRow();
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.change_type(cell_block_type::empty);
        }
        
        theBlock[0].setRow(left_row-1);
        theBlock[0].setCol(left_col);
        theBlock[1].setRow(left_row-1);
        theBlock[1].setCol(left_col+1);
        theBlock[2].setRow(left_row);
        theBlock[2].setCol(left_col+1);
        theBlock[3].setRow(left_row);
        theBlock[3].setCol(left_col+2);
        
        for(auto&a: theBlock){
            a.setState(StateType::no_state);
            a.change_type(this->my_type);
        }
        
        rotateShape = 1;
    }
}


void Z_Block::clockwise(int time){
    counterclockwise(time);
}
