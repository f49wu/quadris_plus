#ifndef Block_h
#define Block_h

#include "block.h"

class Block;
class Cell;

class I_Block: public Block{
    
public:
    I_Block(int level, cell_block_type type);
    ~I_Block();
    void counterclockwise(int time) override;
    void clockwise(int time) override;
};

#endif





