#ifndef cell_h
#define cell_h

#include "subject.h"
#include "block.h"

class Subject;
struct Info;
class TextDisplay;

class Cell: public Subject{
    
    int row;
    int col;
    cell_block_type type = cell_block_type::empty;
    
public:
    void right(int);
    void left(int);
    void down(int);
    Cell(int row, int col, cell_block_type type = cell_block_type::empty);
    ~Cell() override;
    int getCol() const;
    int getRow() const;
    void setCol(int new_col);
    void setRow(int new_row);
    
    // change_type will change the type of that cell
    // and notice all of its observers: TextDisplay, GraphicsDisplay,
    // and Block (if there's a block being attached to it)
    void change_type(cell_block_type new_type);
    Info getInfo() const override;
    
};


#endif
