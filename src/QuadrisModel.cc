#include "QuadrisModel.h"
#include <iostream>
#include <fstream>

using namespace std;

void QuadrisModel::AI_auto_step(){
    this->getHint();
    // make the block disappear and
    // then let it be dropped at the suggested position
    cur_block->disappear();
    cur_block->setTheBlockPos(cur_hint_pos);
    this->drop(1);
}


void QuadrisModel::nonRandom(string file){
    // if it's not in level 3 or 4
    // no effect
    if(level!=3 && level!=4){
        return;
    }
    // check if the file exist
    fstream fs_check;
    
    fs_check.open(file);
    if(!fs_check.is_open()){
        cout<<"ERROR: can not open file: "
        << file <<endl;
    }else{
        fs_check.close();
        this->cur_level->setNoRandom(file);
        this->changeNext();
    }
}


void QuadrisModel::Random(){
    // if it's not in level 3 or 4
    // no effect
    if(level!=3 && level!=4){
        return;
    }
    
    // if the current level is
    // already in random model
    if(cur_level->is_in_random()){
        return;
    }
    
    this->cur_level->setRandom();
    this->changeNext();
}


void QuadrisModel::GameOver(){
    td->clear();
    td->game_over();
    if(!text_model){
        gd->game_over();
    }
    is_game_over = true;
}


bool QuadrisModel::game_over() const{
    return is_game_over;
}


void QuadrisModel::restart(){
    int hi_score = this->getHiScore();
    blocks_on_Grid.clear();
    cur_hint_pos.clear();
    hint_on_board = false;
    td->clear();
    
    // re-initialize the model
    this->init(td, gd, text_model, seed, file_name, start_level);
    this->setHiScore(hi_score);
    return;
}


void QuadrisModel::setHiScore(int hi){
    this->hi_score = hi;
    if(!text_model){
        gd->UpdateInfo();
    }
}


int QuadrisModel::abs(int a){
    if(a < 0){
        return (-1) * a;
    }else{
        return a;
    }
}

// get the sum of the heights of each column
int QuadrisModel::getAggregateHeight(vector<std::vector<Info>>& theGrid, vector<int>& heights){
    
    int col_height = row;
    int i = 0;
    int result = 0;
    
    heights.clear();
    
    
    for(int j = 0; j < col; j++){
        for(i = first_row; i < row; i++){
            if(theGrid[i][j].type != cell_block_type::empty){
                break;
            }
        }
        col_height = row - i;
        heights.emplace_back(col_height);
        result+= col_height;
    }
    
    return result;
}


// get the number of holes in the grid
int QuadrisModel::getNum_Holes(vector<std::vector<Info>>& theGrid){
    int result = 0;
    
    
    for(int j = 0; j < col; j++){
        for(int i = first_row+1; i < row; i++){
            if(theGrid[i][j].type == cell_block_type::empty
               && theGrid[i-1][j].type != cell_block_type::empty){
                result++;
            }
        }
    }
    
    return result;
}


// get the number of complete lines in the grid
int QuadrisModel::getCompleteLine(vector<std::vector<Info>>& theGrid){
    int result = 0;
    
    bool complete_line = true;
    
    for(int i = first_row; i < row; i++){
        
        complete_line = true;
        
        for(int j = 0; j < col; j++){
            if(theGrid[i][j].type == cell_block_type::empty){
                complete_line = false;
                break;
            }
        }
        
        if(complete_line){
            result++;
        }
    }
    
    return result;
}


double QuadrisModel::Move_score(vector<std::vector<Info>>& theGrid){
    int Aggregate_Height = 0;
    int Complete_line = 0;
    int Holes = 0;
    int Bumpiness = 0;
    
    vector<int> heights;
    
    Holes = getNum_Holes(theGrid);
    Complete_line = getCompleteLine(theGrid);
    Aggregate_Height = getAggregateHeight(theGrid, heights);
    
    for(int i = 0; i < 10; i++){
        int diff = abs(heights[i] - heights[i+1]);
        Bumpiness+= diff;
    }
    double score = (-0.1)*Aggregate_Height + 100*Complete_line*Complete_line + (-5)*Holes + (-1)*Bumpiness;
    return score;
}


void QuadrisModel::getHint(){
    this->clearHint();
    
    if(cur_block->is_unmovable()){
        cur_hint_pos = cur_block->Block_Position();
        g->hint(cur_hint_pos);
        hint_on_board = true;
        return;
    }
    
    shared_ptr<Block> test_pos;
    
    if(cur_block->getBlockType() == cell_block_type::I){
        test_pos = make_shared<I_Block>(0, cell_block_type::J);
    }else if(cur_block->getBlockType() == cell_block_type::J){
        test_pos = make_shared<J_Block>(0, cell_block_type::J);
    }else if(cur_block->getBlockType() == cell_block_type::L){
        test_pos = make_shared<L_Block>(0, cell_block_type::L);
    }else if(cur_block->getBlockType() == cell_block_type::O){
        test_pos = make_shared<O_Block>(0, cell_block_type::O);
    }else if(cur_block->getBlockType() == cell_block_type::S){
        test_pos = make_shared<S_Block>(0, cell_block_type::S);
    }else if(cur_block->getBlockType() == cell_block_type::Z){
        test_pos = make_shared<Z_Block>(0, cell_block_type::Z);
    }else if(cur_block->getBlockType() == cell_block_type::T){
        test_pos = make_shared<T_Block>(0, cell_block_type::T);
    }
    
    test_pos->setTheBlockPos(cur_block->Block_Position());
    test_pos->setRotateShape(cur_block->GetRotateShape());
    
    // copy the current grid by 2-dimension vector<Info>
    vector<vector<Info>> theGrid_copy = g->GetTheGrid();
    
    shared_ptr<Block> back_up = cur_block;
    
    cur_block = test_pos;
    
    this->left(9);
    
    int ori_shape = cur_block->GetRotateShape();
    vector<Info> ori_position = cur_block->Block_Position();
    vector<Info> best_pos;
    
    double best_score = 0.0;
    
    this->down(18);
    
    for(auto&a: cur_block->Block_Position()){
        theGrid_copy[a.row][a.col].type = cell_block_type::hint;
    }
    
    best_score = Move_score(theGrid_copy);
    best_pos = test_pos->Block_Position();
    
    for(auto&a: cur_block->Block_Position()){
        theGrid_copy[a.row][a.col].type = cell_block_type::empty;
    }
    
    cur_block->setRotateShape(ori_shape);
    cur_block->setTheBlockPos(ori_position);
    
    
    // go through each possible position
    for(int i = 0; i < 4; i++){
        
        this->clockwise(i);
        
        for(int j = 0; j < col; j++){
            
            int ori_rotated_shape_move_shape = cur_block->GetRotateShape();
            vector<Info> ori_rotated_shape_move_pos = cur_block->Block_Position();
            
            if(i == 0 && j == 0){
                continue;
            }
            
            double cur_move_socre = 0.0;
            
            this->right(j);
            this->down(18);
            
            for(auto&a: cur_block->Block_Position()){
                theGrid_copy[a.row][a.col].type = cell_block_type::hint;
            }
            
            cur_move_socre = Move_score(theGrid_copy);
            
            // compare the socres and update the best position
            if(cur_move_socre > best_score){
                best_score = cur_move_socre;
                best_pos = cur_block->Block_Position();
            }
            
            for(auto&a: cur_block->Block_Position()){
                theGrid_copy[a.row][a.col].type = cell_block_type::empty;
            }
            // set the current block to the original position
            // and origianl rotate status(shape)
            cur_block->setRotateShape(ori_rotated_shape_move_shape);
            cur_block->setTheBlockPos(ori_rotated_shape_move_pos);
        }
        
        // set the current block to the original position
        // and origianl rotate status(shape)
        cur_block->setRotateShape(ori_shape);
        cur_block->setTheBlockPos(ori_position);
        
    }
    cur_block = back_up;
    cur_hint_pos = best_pos;
    g->hint(cur_hint_pos);
    hint_on_board = true;
}


void QuadrisModel::levelUp(int m){
    this->clearHint();
    
    // if the updated level is not valid
    if(m<=0){
        return;
    }
    
    // create the required level
    if(level + m >= 4){
        level = 4;
        cur_level = make_unique<Level_Three>(4);
    }else if(level+m == 1){
        level = 1;
        cur_level = make_unique<Level_One>(1);
        cur_level->SetSeed(seed);
    }else if(level+m == 2){
        level = 2;
        cur_level = make_unique<Level_Two>(2);
        cur_level->SetSeed(seed);
    }else if(level+m == 3){
        level = 3;
        cur_level = make_unique<Level_Three>(3);
        cur_level->SetSeed(seed);
    }
    
    
    if(!text_model){
        gd->UpdateInfo();
    }
}


void QuadrisModel::levelDown(int m){
    this->clearHint();
    // if the updated level is not valid
    if(m<=0){
        return;
    }
    
    // create the required level
    if(level - m <= 0){
        level = 0;
        cur_level = make_unique<Level_Zero>(0, file_name);
        
    }else if(level-m == 1){
        level = 1;
        cur_level = make_unique<Level_One>(1);
        cur_level->SetSeed(seed);
    }else if(level-m == 2){
        level = 2;
        cur_level = make_unique<Level_Two>(2);
        cur_level->SetSeed(seed);
    }else if(level-m == 3){
        level = 3;
        cur_level = make_unique<Level_Three>(3);
        cur_level->SetSeed(seed);
    }
    
    
    
    if(!text_model){
        gd->UpdateInfo();
    }
}


void QuadrisModel::GenerateOne_by_OneBlock(){
    int i = 3;
    
    for(; i< 18; i++){
        if(!g->is_empty_cell(i, 5)){
            break;
        }
    }
    
    i--;
    
    if(g->is_empty_cell(3, 5)){
        // if the top middle cell is empty,
        // that means we can still drop one by one block
        shared_ptr<ObyO_Block> OneByOne = make_shared<ObyO_Block>(level, cell_block_type::ObyO, i);
        g->setNewBlock(OneByOne);
        blocks_on_Grid.emplace_back(OneByOne);
    }else{
        // if the top middle cell is not empty,
        // that means game is over
        GameOver();
    }
}


void QuadrisModel::swap_cur_Block(cell_block_type type){
    this->clearHint();
    
    if(type == cur_block->getBlockType()){
        return;
    }
    
    vector<Info> ori_pos = cur_block->Block_Position();
    
    shared_ptr<Block>new_block;
    
    int ori_level = cur_block->getLevel();
    
    // create corresponding block
    if(type == cell_block_type::I){
        new_block = make_shared<I_Block>(ori_level, cell_block_type::I);
    }else if(type == cell_block_type::J){
        new_block = make_shared<J_Block>(ori_level, cell_block_type::J);
    }else if(type == cell_block_type::L){
        new_block = make_shared<L_Block>(ori_level, cell_block_type::L);
    }else if(type == cell_block_type::O){
        new_block = make_shared<O_Block>(ori_level, cell_block_type::O);
    }else if(type == cell_block_type::S){
        new_block = make_shared<S_Block>(ori_level, cell_block_type::S);
    }else if(type == cell_block_type::Z){
        new_block = make_shared<Z_Block>(ori_level, cell_block_type::Z);
    }else if(type == cell_block_type::T){
        new_block = make_shared<T_Block>(ori_level, cell_block_type::T);
    }
    
    int left_col = ori_pos[0].col;
    int left_row = ori_pos[0].row;
    
    for(auto& a: ori_pos){
        if(left_col > a.col){
            left_col = a.col;
        }
        if(left_row < a.row){
            left_row = a.row;
        }
    }
    
    new_block->right(left_col);
    
    if(new_block->getBlockType() == cell_block_type::I){
        new_block->down(left_row - 3);
    }else{
        if(left_row >= 4){
            new_block->down(left_row - 4);
        }
    }
    
    // check if the swapped block can fit in current position
    if(canMove(new_block->Block_Position())){
        cur_block->disappear();
        cur_block = new_block;
        new_block->setTextDisplayer(td);
        if(!text_model){
            new_block->setGraphicsDisplayer(gd);
        }
        return;
    }
    return;
}


void QuadrisModel::clearHint(){
    if(hint_on_board){
        g->clear_hint(cur_hint_pos);
        cur_block->appear();
        hint_on_board = false;
    }
}


void QuadrisModel::drop(int m){
    this->clearHint();
    
    for(int i = 0; i < m; i++){
        
        // make current block move downward
        // by 18 positions, that is make the
        // current block at the lowest position
        this->down(row);
        
        // then we just drop the block
        cur_block->drop();
        
        // set the new block on the grid
        g->setNewBlock(cur_block);
        blocks_on_Grid.emplace_back(cur_block);
        clear_rows();
        
        // generating the one-by-one block
        if(level == 4 && num_without_clear >= 5){
            this->GenerateOne_by_OneBlock();
            num_without_clear = 0;
        }
        
        if(isOver()){
            this->GameOver();
            return;
        }
        
        NextBlock();
    }
}


void QuadrisModel::setSeqFile(string file_name){
    this->clearHint();
    cur_level->setSequence(file_name);
}


bool QuadrisModel::isOver(){
    
    // using the test_block to check if there's enough space for
    // the next current block
    unique_ptr<Block> test_block;
    
    if(next_block == cell_block_type::I){
        test_block = make_unique<I_Block>(level, cell_block_type::I);
    }else if(next_block == cell_block_type::J){
        test_block = make_unique<J_Block>(level, cell_block_type::J);
    }else if(next_block == cell_block_type::L){
        test_block = make_unique<L_Block>(level, cell_block_type::L);
    }else if(next_block == cell_block_type::O){
        test_block = make_unique<O_Block>(level, cell_block_type::O);
    }else if(next_block == cell_block_type::S){
        test_block = make_unique<S_Block>(level, cell_block_type::S);
    }else if(next_block == cell_block_type::Z){
        test_block = make_unique<Z_Block>(level, cell_block_type::Z);
    }else if(next_block == cell_block_type::T){
        test_block = make_unique<T_Block>(level, cell_block_type::T);
    }
    
    vector<Info> pos = test_block->Block_Position();
    
    for(auto& a: pos){
        if(!g->is_empty_cell(a.row, a.col)){
            return true;
        }
    }
    
    return false;
}


void QuadrisModel::cclockwise(int m){
    this->clearHint();
    if(m<=0){
        return;
    }
    
    // if the block is unmovable
    if(cur_block->is_unmovable()){
        return;
    }
    
    // using test_pos to check if there's enough
    // space to rotate
    unique_ptr<Block> test_pos;
    
    if(cur_block->getBlockType() == cell_block_type::I){
        test_pos = make_unique<I_Block>(0, cell_block_type::I);
    }else if(cur_block->getBlockType() == cell_block_type::J){
        test_pos = make_unique<J_Block>(0, cell_block_type::J);
    }else if(cur_block->getBlockType() == cell_block_type::L){
        test_pos = make_unique<L_Block>(0, cell_block_type::L);
    }else if(cur_block->getBlockType() == cell_block_type::O){
        test_pos = make_unique<O_Block>(0, cell_block_type::O);
    }else if(cur_block->getBlockType() == cell_block_type::S){
        test_pos = make_unique<S_Block>(0, cell_block_type::S);
    }else if(cur_block->getBlockType() == cell_block_type::Z){
        test_pos = make_unique<Z_Block>(0, cell_block_type::Z);
    }else if(cur_block->getBlockType() == cell_block_type::T){
        test_pos = make_unique<T_Block>(0, cell_block_type::T);
    }
    
    test_pos->setTheBlockPos(cur_block->Block_Position());
    test_pos->setRotateShape(cur_block->GetRotateShape());
    
    test_pos->counterclockwise(m);
    
    if(canMove(test_pos->Block_Position())){
        cur_block->counterclockwise(m);
        if(cur_block->heavy()){
            this->autodown();
        }
        return;
    }
    
    int far_distance = m;
    
    for(int i = 0; i < m; i++){
        test_pos->clockwise(1);
        far_distance--;
        if(canMove(test_pos->Block_Position())){
            cur_block->counterclockwise(far_distance);
            break;
        }
    }
    
    if(cur_block->heavy()){
        this->autodown();
    }
    
}


void QuadrisModel::clockwise(int m){
    
    this->clearHint();
    
    if(m<=0){
        return;
    }
    
    // if the block is unmovable
    if(cur_block->is_unmovable()){
        return;
    }
    
    // using test_pos to check if there's enough
    // space to rotate
    unique_ptr<Block> test_pos;
    
    if(cur_block->getBlockType() == cell_block_type::I){
        test_pos = make_unique<I_Block>(0, cell_block_type::I);
    }else if(cur_block->getBlockType() == cell_block_type::J){
        test_pos = make_unique<J_Block>(0, cell_block_type::J);
    }else if(cur_block->getBlockType() == cell_block_type::L){
        test_pos = make_unique<L_Block>(0, cell_block_type::L);
    }else if(cur_block->getBlockType() == cell_block_type::O){
        test_pos = make_unique<O_Block>(0, cell_block_type::O);
    }else if(cur_block->getBlockType() == cell_block_type::S){
        test_pos = make_unique<S_Block>(0, cell_block_type::S);
    }else if(cur_block->getBlockType() == cell_block_type::Z){
        test_pos = make_unique<Z_Block>(0, cell_block_type::Z);
    }else if(cur_block->getBlockType() == cell_block_type::T){
        test_pos = make_unique<T_Block>(0, cell_block_type::T);
    }
    
    
    test_pos->setTheBlockPos(cur_block->Block_Position());
    test_pos->setRotateShape(cur_block->GetRotateShape());
    
    test_pos->clockwise(m);
    
    
    if(canMove(test_pos->Block_Position())){
        cur_block->clockwise(m);
        if(cur_block->heavy()){
            this->autodown();
        }
        return;
    }
    
    int far_distance = m;
    
    for(int i = 0; i < m; i++){
        test_pos->counterclockwise(1);
        far_distance--;
        if(canMove(test_pos->Block_Position())){
            cur_block->clockwise(far_distance);
            break;
        }
    }
    
    if(cur_block->heavy()){
        this->autodown();
    }
}


void QuadrisModel::changeNext(){
    // change the next block according
    // to current level
    next_block = cur_level->NextBlock();
    level_of_next_block = level;
    
    td->ShowNextBlock(next_block);
    
    if(!text_model){
        gd->showNext(next_block);
    }
}


void QuadrisModel::NextBlock(){
    
    // create current block differently
    if(next_block == cell_block_type::I){
        cur_block = make_shared<I_Block>(level_of_next_block, cell_block_type::I);
    }else if(next_block == cell_block_type::J){
        cur_block = make_shared<J_Block>(level_of_next_block, cell_block_type::J);
    }else if(next_block == cell_block_type::L){
        cur_block = make_shared<L_Block>(level_of_next_block, cell_block_type::L);
    }else if(next_block == cell_block_type::O){
        cur_block = make_shared<O_Block>(level_of_next_block, cell_block_type::O);
    }else if(next_block == cell_block_type::S){
        cur_block = make_shared<S_Block>(level_of_next_block, cell_block_type::S);
    }else if(next_block == cell_block_type::Z){
        cur_block = make_shared<Z_Block>(level_of_next_block, cell_block_type::Z);
    }else if(next_block == cell_block_type::T){
        cur_block = make_shared<T_Block>(level_of_next_block, cell_block_type::T);
    }
    
    cur_block->setTextDisplayer(td);
    
    if(!text_model){
        cur_block->setGraphicsDisplayer(gd);
    }
    
    next_block = cur_level->NextBlock();
    level_of_next_block = level;
    
    td->ShowNextBlock(next_block);
    
    if(!text_model){
        gd->showNext(next_block);
    }
    
}


void QuadrisModel::positionUp(vector<Info> & test_pos, int m){
    for(auto &a: test_pos){
        a.row-=m;
    }
}


void QuadrisModel::positionDown(vector<Info> & test_pos, int m){
    for(auto &a: test_pos){
        a.row+=m;
    }
}


void QuadrisModel::positionLeft(vector<Info> & test_pos, int m){
    for(auto &a: test_pos){
        a.col-=m;
    }
}


void QuadrisModel::positionRight(vector<Info> & test_pos, int m){
    for(auto &a: test_pos){
        a.col+=m;
    }
}


bool QuadrisModel::canMove(vector<Info> test_positions){
    
    for(auto& a: test_positions){
        if(((a.col < col && a.col >= 0)&&(a.row < row && a.row >= 0))){
            if(!g->is_empty_cell(a.row, a.col)){
                return false;
            }
        }else{
            return false;
        }
    }
    
    return true;
}


void QuadrisModel::autodown(){
    
    if(cur_block->is_unmovable()){
        return;
    }
    
    // using the test_pos to check if there's
    // space to move downward
    vector<Info> test_pos = cur_block->Block_Position();
    
    positionDown(test_pos, 1);
    
    if(canMove(test_pos)){
        cur_block->down(1);
        return;
    }else{
        if(cur_block->heavy()){
            cur_block->setUnmoveable();
        }
    }
}


void QuadrisModel::down(int m){
    this->clearHint();
    
    if(m<=0){
        return;
    }
    
    if(cur_block->is_unmovable()){
        return;
    }
    
    vector<Info> test_pos = cur_block->Block_Position();
    
    int i = 0;
    
    for(; i < m; i++){
        positionDown(test_pos, 1);
        if(!canMove(test_pos)){
            break;
        }
    }
    
    
    cur_block->down(i);
    
    
    if(cur_block->heavy()){
        this->autodown();
    }
    
}


void QuadrisModel::right(int m){
    this->clearHint();
    
    if(m<=0){
        return;
    }
    
    if(cur_block->is_unmovable()){
        return;
    }
    
    vector<Info> test_pos = cur_block->Block_Position();
    
    int i = 0;
    
    for(; i < m; i++){
        positionRight(test_pos, 1);
        if(!canMove(test_pos)){
            break;
        }
    }
    
    cur_block->right(i);
    
    if(cur_block->heavy()){
        this->autodown();
    }
    
}


void QuadrisModel::left(int m){
    
    this->clearHint();
    
    if(m<=0){
        return;
    }
    
    if(cur_block->is_unmovable()){
        return;
    }
    
    vector<Info> test_pos = cur_block->Block_Position();
    
    int i = 0;
    
    for(; i < m; i++){
        positionLeft(test_pos, 1);
        if(!canMove(test_pos)){
            break;
        }
    }
    
    cur_block->left(i);
    
    if(cur_block->heavy()){
        this->autodown();
    }
}


shared_ptr<TextDisplay> QuadrisModel::getTextDisplay() const{
    return td;
}


shared_ptr<GraphicsDisplay> QuadrisModel::getGraphicsDisplay() const{
    return gd;
}


int QuadrisModel::getHiScore() const{
    return hi_score;
}


int QuadrisModel::getScore() const{
    return cur_score;
}


int QuadrisModel::getLevel() const{
    return level;
}


int QuadrisModel::sqrt(int num){
    return num*num;
}


void QuadrisModel::update_socre(int add){
    cur_score+=add;
    
    if(hi_score < cur_score){
        hi_score = cur_score;
    }
    
    if(!text_model){
        gd->UpdateInfo();
    }
}


void QuadrisModel::clear_rows(){
    int row_cleared = g->clear_row();
    
    if(row_cleared != 0){
        int score_point = sqrt(level + row_cleared);
        
        for(int i = 0; i < (int)blocks_on_Grid.size(); i++){
            if(blocks_on_Grid[i]->empty()){
                score_point+=sqrt(blocks_on_Grid[i]->getLevel() + 1);
                blocks_on_Grid.erase(blocks_on_Grid.begin() + i);
                i--;
            }
        }
        
        update_socre(score_point);
        
        // if level is 4, reset the counter
        if(level == 4){
            num_without_clear = 0;
        }
        
    }else{
        // if there's no row being cleard and
        // the level is 4, then update the counter
        if(cur_block->getLevel() == 4){
            num_without_clear++;
        }
    }
}


QuadrisModel::~QuadrisModel(){
}


void QuadrisModel::init(shared_ptr<TextDisplay> td, shared_ptr<GraphicsDisplay> gd, bool text_model, int seed, string source_file, int level){
    this->td = td;
    this->gd = gd;
    
    is_game_over = false;
    
    if(level < 0 || level > 4){
        cout<<"invalid startlevel!"<<endl;
        level = 0;
    }
    
    this->seed = seed;
    this->level = level;
    this->start_level = level;
    this->text_model = text_model;
    file_name = source_file;
    num_without_clear = 0;
    
    // initialize the grid
    g = make_unique<Grid>();
    g->init();
    
    if(!text_model){
        g->setGraphicsDisplayer(gd);
    }
    g->setTextDispalyer(td);
    
    cur_score = 0;
    hi_score = 0;
    blocks_on_Grid.clear();
    
    // create the level according to the given
    // startlevel
    if(level == 0){
        this->cur_level = make_unique<Level_Zero>(0, file_name);
    }else if(level == 1){
        this->cur_level = make_unique<Level_One>(1);
    }else if(level == 2){
        this->cur_level = make_unique<Level_Two>(2);
    }else if(level == 3 || level == 4){
        this->cur_level = make_unique<Level_Three>(3);
    }
    cur_level->SetSeed(this->seed);
    
    // initialize the GraphicsDisplayer
    if(!text_model){
        gd->init();
    }
    
    next_block = this->cur_level->NextBlock();
    level_of_next_block = this->level;
    
    this->NextBlock();
}
