#ifndef Level_Three_h
#define Level_Three_h

#include "level.h"

class Block;
class Level;

class Level_Three: public Level{
  
    std::vector<cell_block_type> sequence;
    std::vector<std::string> NoRandomSequence;
    // len of the no_random sequence
    int len_NoRandom;
    // index of no_random sequence
    int index = 0;
    std::string NoRandomfile = "";
    
    cell_block_type _NextBlock() override;
    void _setRandom() override;
    void _setNoRandom(std::string file) override;
    
public:
    Level_Three(int level);
    ~Level_Three();
};

#endif 
