#include "Quadris_Controller.h"
#include <sstream>
#include <fstream>

using namespace std;

Quadris_Controller::Quadris_Controller(){
}


Quadris_Controller::Quadris_Controller(shared_ptr<GraphicsDisplay> gd,
                                       shared_ptr<QuadrisModel> p, shared_ptr<TextDisplay> td,
                                       bool textmodel,int seed, std::string sourcefile,
                                       int startlevel):model{p}{
    model->init(td, gd, textmodel, seed, sourcefile, startlevel);
    
    shared_ptr<TextDisplay> temp = model->getTextDisplay();
    cout<<*temp;
}


void Quadris_Controller::start(){
    
    string raw_cmd;
    
    if(ai_model){
        AI_model->AI_auto_step();
    }
    
    while(cin>>raw_cmd){
        cmdMatch(raw_cmd);
    }
}


void Quadris_Controller::setAI(shared_ptr<QuadrisModel> AI){
    ai_model = true;
    this->AI_model = AI;
    cout<<*(AI_model->getTextDisplay());
}


void Quadris_Controller::readfile(string file_name){
    fstream fs;
    
    fs.open(file_name);
    
    // if the file open fail
    if(!fs.is_open()){
        cout<<"CAN NOT OPEN FILE by Controller"<<endl;
        return;
    }
    
    string raw_cmd;
    
    while(fs>>raw_cmd){
        cmdMatch(raw_cmd);
    }
    
    fs.close();
}


void Quadris_Controller::cmdMatch(string row_cmd){
    
    stringstream temp;
    stringstream backup;
    temp<<row_cmd;
    backup<<row_cmd;
    
    // default prefix is 1
    int prefix = 1;
    
    string left = "left";
    string levelup = "levelup";
    string leveldown = "leveldown";
    
    string right = "right";
    string random = "random";
    string restart = "restart";
    
    string sequence = "sequence";
    
    string clockwise = "clockwise";
    string counterclockwise = "counterclockwise";
    
    string drop = "drop";
    string down = "down";
    
    string norandom = "norandom";
    
    string hint = "hint";
    
    string cmd = "";
    
    if(temp>>prefix){
        
        if(prefix <= 0){
            return;
        }
        
        temp >> cmd;
    }else{
        backup >> cmd;
        prefix = 1;
    }
    
    
    if(model->game_over()){
        if(cmd.size() <= restart.size() && cmd.size() >= 2
           && cmd == restart.substr(0,cmd.size())){
            if(ai_model){
                AI_model->restart();
                shared_ptr<GraphicsDisplay> temp_gd = model->getGraphicsDisplay();
                shared_ptr<GraphicsDisplay> temp_gd_ai = AI_model->getGraphicsDisplay();
                temp_gd->setAIdisplayer(temp_gd_ai->GetWindow()); 
            }
            model->restart();
        }
        return;
    }
    
    // matching command
    if(cmd == "lef" || cmd == "left"){
        model->left(prefix);
    }else if(cmd.size() <= levelup.size() && cmd.size() >= 6 && cmd == levelup.substr(0,cmd.size())){
        model->levelUp(prefix);
        if(ai_model){
            AI_model->levelUp(prefix);
        }
    }else if(cmd.size() <= leveldown.size() && cmd.size() >= 6 && cmd == leveldown.substr(0,cmd.size())){
        model->levelDown(prefix);
        if(ai_model){
            AI_model->levelDown(prefix);
        }
    }else if(cmd.size() <= right.size() && cmd.size() >= 2 && cmd == right.substr(0,cmd.size())){
        model->right(prefix);
    }else if(cmd.size() <= random.size() && cmd.size() >= 2 && cmd == random.substr(0,cmd.size())){
        model->Random();
        if(ai_model){
            AI_model->Random();
        }
    }else if(cmd.size() <= restart.size() && cmd.size() >= 2 && cmd == restart.substr(0,cmd.size())){
        if(ai_model){
            AI_model->restart();
            shared_ptr<GraphicsDisplay> temp_gd = model->getGraphicsDisplay();
            shared_ptr<GraphicsDisplay> temp_gd_ai = AI_model->getGraphicsDisplay();
            temp_gd->setAIdisplayer(temp_gd_ai->GetWindow()); 
        }
        model->restart();
        if(ai_model){
            AI_model->AI_auto_step();
        }
    }else if(cmd.size() <= sequence.size() && cmd.size() >= 1 && cmd == sequence.substr(0,cmd.size())){
        string file_name;
        cin>>file_name;
        readfile(file_name);
    }else if(cmd.size() <= clockwise.size() && cmd.size() >= 2 && cmd == clockwise.substr(0,cmd.size())){
        model->clockwise(prefix);
    }else if(cmd.size() <= counterclockwise.size() && cmd.size() >= 2 && cmd == counterclockwise.substr(0,cmd.size())){
        model->cclockwise(prefix);
    }else if(cmd.size() <= drop.size() && cmd.size() >= 2 && cmd == drop.substr(0,cmd.size())){
        model->drop(prefix);
        if(ai_model){
            for(int i = 0; i < prefix; i++){
                AI_model->AI_auto_step();
            }
        }
    }else if(cmd.size() <= down.size() && cmd.size() >= 2 && cmd == down.substr(0,cmd.size())){
        model->down(prefix);
    }else if(cmd.size() <= norandom.size() && cmd.size() >= 1 && cmd == norandom.substr(0,cmd.size())){
        string file_name;
        cin>>file_name;
        model->nonRandom(file_name);
        if(ai_model){
            AI_model->nonRandom(file_name);
        }
    }else if(cmd.size() <= hint.size() && cmd.size() >= 1 && cmd == hint.substr(0,cmd.size())){
        model->getHint();
    }else if(cmd == "I"){
        model->swap_cur_Block(cell_block_type::I);
    }else if(cmd == "J"){
        model->swap_cur_Block(cell_block_type::J);
    }else if(cmd == "L"){
        model->swap_cur_Block(cell_block_type::L);
    }else if(cmd == "O"){
        model->swap_cur_Block(cell_block_type::O);
    }else if(cmd == "S"){
        model->swap_cur_Block(cell_block_type::S);
    }else if(cmd == "Z"){
        model->swap_cur_Block(cell_block_type::Z);
    }else if(cmd == "T"){
        model->swap_cur_Block(cell_block_type::T);
    }else if(cmd == "auto"){
        model->AI_auto_step();
        if(ai_model){
            AI_model->AI_auto_step();
        }
    }else{
        cout<<"5:Invalid Input!"<<endl;
    }
    
    shared_ptr<TextDisplay>temp_td = model->getTextDisplay();
    cout<<*temp_td;
    
    if(ai_model){
        cout<<*(AI_model->getTextDisplay());
    }
}
