#ifndef O_Blcok_h
#define O_Blcok_h

#include "block.h"

class Block;
class Cell;

class O_Block: public Block{
    
public:
    O_Block(int level, cell_block_type type);
    ~O_Block();
    void counterclockwise(int time) override;
    void clockwise(int time) override;
};
#endif
