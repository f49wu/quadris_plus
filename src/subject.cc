#include "subject.h"

void Subject::attach(std::shared_ptr<Observer> o) {
    observers.emplace_back(o);
}


void Subject::setState(StateType newS) {
    my_state = newS;
}

 
StateType Subject::getState() const{
    return my_state;
} 


std::shared_ptr<Observer> Subject::detach(){
    std::shared_ptr<Observer> result = observers.back();
    observers.pop_back();
    return result;
}


void Subject::notifyObservers() {
    for (auto &ob : observers){
        ob->notify(*this);
    }
}


Subject::~Subject(){
}

